# Doki

This is a Discord bot that takes care of people that use the `Invisible` status while chatting or trying to join voice channels.

Whenever a user is offline (or invisible) they can't:
- Send text messages (they get deleted immediately)
- Join voice channels (they get disconnected immediately)
- Add reactions to messages (they get removed immediately)

Changing the status to invisible while begin in a voice channel also causes a disconnect.

## Installation

Use [my Docker container](https://hub.docker.com/repository/docker/derenderkeks/doki):
```shell
docker run -e DISCORD_TOKEN=your-token-here derenderkeks/doki
```

Or compile it manually, see the [Dockerfile](Dockerfile) for instructions.

You need to specify the Discord Bot Token with the environment variable `DISCORD_TOKEN`.

The bot prints out an invitation URL every time it starts.

## License

[AGPL-3.0](License)