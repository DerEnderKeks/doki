package main

import (
	"sync"
	"time"
)

type RateLimiter struct {
	usersMap map[string]map[WarningMessageType]time.Time
	mapLock  sync.Mutex
	delay    uint
}

func NewRateLimiter(delay uint) *RateLimiter {
	rl := &RateLimiter{}
	rl.usersMap = make(map[string]map[WarningMessageType]time.Time)
	rl.delay = delay
	return rl
}

func (rateLimiter *RateLimiter) IsLimited(userID string, msgType WarningMessageType) bool {
	rateLimiter.mapLock.Lock()
	defer rateLimiter.mapLock.Unlock()
	now := time.Now()
	return now.Before(rateLimiter.usersMap[userID][msgType])
}

func (rateLimiter *RateLimiter) Limit(userID string, msgType WarningMessageType) {
	rateLimiter.mapLock.Lock()
	defer rateLimiter.mapLock.Unlock()
	limitUntil := time.Now().Add(time.Duration(rateLimiter.delay) * time.Second)
	if rateLimiter.usersMap[userID] == nil {
		rateLimiter.usersMap[userID] = make(map[WarningMessageType]time.Time)
	}
	rateLimiter.usersMap[userID][msgType] = limitUntil
}

func (rateLimiter *RateLimiter) CheckAndLimit(userID string, msgType WarningMessageType) bool {
	isLimited := rateLimiter.IsLimited(userID, msgType)
	if !isLimited {
		rateLimiter.Limit(userID, msgType)
	}
	return isLimited
}
