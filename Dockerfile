FROM golang:1.16-alpine as builder

RUN apk add --no-cache \
    upx

WORKDIR /app

COPY ./go.* ./
RUN go mod download
RUN go mod verify

COPY . .

RUN GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o app
RUN upx app

# Final image
FROM alpine
LABEL maintainer='DerEnderKeks'

COPY --from=builder /app/app /app/

USER 65534

ENV DISCORD_TOKEN ""

ENTRYPOINT ["/app/app"]
