package main

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type WarningMessageType uint8

const (
	WarningMessageSendingMessage WarningMessageType = iota
	WarningMessageConnectingToVoice
	WarningMessageConnectedToVoice
	WarningMessageReacting
)

var rateLimiter *RateLimiter

func main() {
	rand.Seed(time.Now().Unix())
	rateLimiter = NewRateLimiter(15)
	token := os.Getenv("DISCORD_TOKEN")
	if len(token) == 0 {
		log.Print("DISCORD_TOKEN env var is missing :(")
		os.Exit(1)
	}
	discord, err := discordgo.New("Bot " + token)
	if err != nil {
		log.Println("Failed to init Discord: ", err)
		os.Exit(1)
	}
	discord.Identify.Intents = discordgo.IntentsGuilds |
		discordgo.IntentsGuildMembers |
		discordgo.IntentsGuildMessages |
		discordgo.IntentsGuildPresences |
		discordgo.IntentsGuildVoiceStates |
		discordgo.IntentsGuildMessageReactions
	discord.AddHandler(ready)
	discord.AddHandler(messageCreate)
	discord.AddHandler(voiceStateUpdate)
	discord.AddHandler(presenceUpdate)
	discord.AddHandler(guildCreate)
	discord.AddHandler(reactionAdded)

	defer discord.Close()
	err = discord.Open()
	if err != nil {
		log.Println("Error opening Discord session: ", err)
		os.Exit(1)
	}
	log.Println("Doki is now running. Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}

func ready(s *discordgo.Session, r *discordgo.Ready) {
	log.Printf("Invite URL: https://discord.com/oauth2/authorize?client_id=%s&scope=bot&permissions=8", s.State.User.ID)
}

func isOffline(s *discordgo.Session, guildID string, userID string) (bool, error) {
	presence, err := s.State.Presence(guildID, userID)
	if err == discordgo.ErrStateNotFound {
		// TODO request state? stick with assuming offline?
		return true, nil
	} else if err != nil {
		return false, err
	}
	return presence.Status == discordgo.StatusOffline, nil
}

func sendWarningMessage(s *discordgo.Session, userID string, message WarningMessageType) {
	if rateLimiter.CheckAndLimit(userID, message) {
		return
	}
	dmc, err := s.UserChannelCreate(userID)
	if err != nil {
		log.Println("Failed to get DM channel: ", err)
		return
	}
	emojis := []string{
		"face_with_raised_eyebrow",
		"rolling_eyes",
		"confused",
		"unamused",
		"slight_frown",
		"frowning",
		"frowning2",
		"person_facepalming",
		"neutral_face",
		"expressionless",
		"face_vomiting",
		"thinking",
	}
	emoji := emojis[rand.Intn(len(emojis))]
	msg := fmt.Sprintf(":%s: It looks like you ", emoji)
	switch message {
	case WarningMessageConnectingToVoice:
		msg += "were offline while connecting to a voice channel"
	case WarningMessageConnectedToVoice:
		msg += "went offline while connected to a voice channel"
	case WarningMessageReacting:
		msg += "were offline while reacting to a message"
	default:
		msg += "were offline while sending a message"
	}
	msg += ". Try again while online ;)"
	_, err = s.ChannelMessageSend(dmc.ID, msg)
	if err != nil {
		log.Println("Failed to send DM: ", err)
	}
}

func reactionAdded(s *discordgo.Session, r *discordgo.MessageReactionAdd) {
	offline, err := isOffline(s, r.GuildID, r.UserID)
	if err != nil {
		log.Println("Failed to check if user is offline: ", err)
		return
	}
	if !offline {
		return
	}
	emojiID := r.Emoji.ID
	if len(emojiID) == 0 {
		emojiID = r.Emoji.Name
	}
	err = s.MessageReactionRemove(r.ChannelID, r.MessageID, emojiID, r.UserID)
	if err != nil {
		log.Println("Failed to remove reaction: ", err)
		return
	}
	sendWarningMessage(s, r.UserID, WarningMessageReacting)
}

func presenceUpdate(s *discordgo.Session, p *discordgo.PresenceUpdate) {
	offline, err := isOffline(s, p.GuildID, p.User.ID)
	if err != nil {
		log.Println("Failed to check if user is offline: ", err)
		return
	}
	if !offline {
		return
	}
	time.Sleep(5 * time.Second) // Don't message people than closed the client
	g, err := s.State.Guild(p.GuildID)
	if err != nil {
		log.Println("Failed to find guild for presence update: ", err)
		return
	}
	for _, vs := range g.VoiceStates {
		if vs.UserID == p.User.ID {
			err = s.GuildMemberMove(p.GuildID, p.User.ID, nil)
			if err != nil {
				log.Println("Failed to disconnect user from voice chat: ", err)
			}
			sendWarningMessage(s, p.User.ID, WarningMessageConnectedToVoice)
			return
		}
	}
}

func guildCreate(s *discordgo.Session, c *discordgo.GuildCreate) {
	for _, vs := range c.Guild.VoiceStates {
		offline, err := isOffline(s, c.Guild.ID, vs.UserID)
		if err != nil || !offline {
			continue
		}

		err = s.GuildMemberMove(c.Guild.ID, vs.UserID, nil)
		if err != nil {
			log.Println("Failed to disconnect user from voice chat: ", err)
		}
		sendWarningMessage(s, vs.UserID, WarningMessageConnectedToVoice)
		return
	}
}

func voiceStateUpdate(s *discordgo.Session, u *discordgo.VoiceStateUpdate) {
	if len(u.ChannelID) == 0 {
		return
	}
	offline, err := isOffline(s, u.GuildID, u.UserID)
	if err != nil {
		log.Println("Failed to check if user is offline: ", err)
		return
	}
	if !offline {
		return
	}
	err = s.GuildMemberMove(u.GuildID, u.UserID, nil)
	if err != nil {
		log.Println("Failed to disconnect user from voice chat: ", err)
	}
	sendWarningMessage(s, u.UserID, WarningMessageConnectingToVoice)
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	offline, err := isOffline(s, m.GuildID, m.Author.ID)
	if err != nil {
		log.Println("Failed to check if user is offline: ", err)
		return
	}
	if !offline {
		return
	}
	err = s.ChannelMessageDelete(m.ChannelID, m.Message.ID)
	if err != nil {
		log.Println("Failed to delete message: ", err)
		return
	}
	sendWarningMessage(s, m.Author.ID, WarningMessageSendingMessage)
}
